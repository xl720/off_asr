/*
 * @Author: your name
 * @Date: 2021-03-06 11:43:50
 * @LastEditTime: 2021-03-07 11:43:54
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \esp-adf\examples\myapp\off_asr\main\audio_tone_uri.c
 */
#include "audio_tone_uri.h"

const char* tone_uri[]={
   "flash://tone/0_air_close.mp3.mp3",
   "flash://tone/1_air_set_ok.mp3",
   "flash://tone/2_ble_conn_succe.mp3",
   "flash://tone/3_ble_disconn.mp3",
   "flash://tone/4_set_timer.mp3",
   "flash://tone/5_wifi_con.mp3",
   "flash://tone/6_wifi_disc.mp3",
   "flash://tone/7_wozai.mp3",
};
